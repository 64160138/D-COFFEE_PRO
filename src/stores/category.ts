import type { Category } from "@/Types/Category";
import { defineStore } from "pinia";
import { ref } from "vue";
import StoreService from "@/services/store";
import { useMessageStore } from "./message";
import CategoryService from "@/services/category";
export const useCatagory = defineStore("category", () => {
  const messageStore = useMessageStore();
  const category = ref<Category[]>([]);
  const editedStore = ref<Category>({
    id: -1,
    name: "",
  });
  async function getCategory() {
    try {
      const res = await CategoryService.getCategory();
      category.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Category ได้");
    }
  }

  return {
    category,
    getCategory,
    editedStore,
  };
});
