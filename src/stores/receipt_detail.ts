import type { ReceiptDetail } from "@/Types/receiptDetail";
import { defineStore } from "pinia";
import { computed, ref } from "vue";
import { useCoffeeStore } from "./menu";
import ReceiptDetailService from "@/services/receipt_detail";
import { useMessageStore } from "./message";
export const useReceiptDetailStore = defineStore("receiptdetail", () => {
  const useCoffee = useCoffeeStore();
  const rec_detail = ref<ReceiptDetail[]>([]);
  const dialog = ref(true);
  const messageStore = useMessageStore();
  const editReceiptDetail = ref<ReceiptDetail>({
    id: -1,
    name: "",
    amount: 0,
    price: 0,
    total: 0,
    type: "",
    category: "",
  });
  const addReceiptDetail = (id: number, type: string | null) => {
    console.log(rec_detail.value);
    const index = useCoffee.coffees.findIndex((item) => item.id == id);
    const name = useCoffee.coffees[index].name;
    const category = useCoffee.coffees[index].type;

    let price = 0;
    if (type === "ร้อน") {
      price = 40;
    } else if (type === "เย็น") {
      price = 45;
    } else if (type === "ปั่น") {
      price = 55;
    } else {
      price = useCoffee.coffees[index].price; // fallback to default price if type is not recognized
    }

    const existingIndex = rec_detail.value.findIndex(
      (item) => item.id === id && item.type === type
    );
    if (existingIndex !== -1) {
      // Item with the same id and type already exists in the array, so increment its amount property.
      rec_detail.value[existingIndex].amount++;
      rec_detail.value[existingIndex].total =
        rec_detail.value[existingIndex].amount * price;
    } else {
      // Item does not exist in the array, so add it with the provided id and set amount to 1.
      rec_detail.value.push({
        id: id,
        name: name,
        amount: 1,
        price: price,
        total: price,
        type: type,
        category: category,
      });
    }
  };

  const DelMenu = (id: number) => {
    const index = rec_detail.value.findIndex((item) => item.id === id);
    if (rec_detail.value[index].amount == 1) {
      rec_detail.value.splice(index, 1);
    } else {
      rec_detail.value[index].amount--;
      rec_detail.value[index].total =
        rec_detail.value[index].price * rec_detail.value[index].amount;
    }
  };
  const AddMenu = (id: number) => {
    const index = rec_detail.value.findIndex((item) => item.id === id);
    if (index !== -1) {
      rec_detail.value[index].amount++;
      rec_detail.value[index].total =
        rec_detail.value[index].price * rec_detail.value[index].amount;
    }
  };

  const deleteOrder = () => {
    rec_detail.value.pop();
  };

  const increment = (name: string, type: string) => {
    const index = rec_detail.value.findIndex(
      (item) => item.name === name && item.type === type
    );
    if (index !== -1) {
      // Item exists in the array, so increment its amount property.
      rec_detail.value[index].amount++;
      rec_detail.value[index].total =
        rec_detail.value[index].price * rec_detail.value[index].amount;
    } else {
      // Item does not exist in the array.
      // You may want to add an error message or do something else here.
    }
  };

  const decrement = (name: string, type: string) => {
    const index = rec_detail.value.findIndex(
      (item) => item.name === name && item.type === type
    );
    if (index !== -1) {
      // Item exists in the array, so decrement its amount property.
      rec_detail.value[index].amount--;
      rec_detail.value[index].total =
        rec_detail.value[index].price * rec_detail.value[index].amount;
      if (rec_detail.value[index].amount === 0) {
        // If the amount becomes 0, remove the item from the array.
        rec_detail.value.splice(index, 1);
      }
    } else {
      // Item does not exist in the array.
      // You may want to add an error message or do something else here.
    }
  };
  const allPrice = computed(() => {
    return rec_detail.value.reduce((total, item) => total + item.total, 0);
  });

  const clearReceipt = () => {
    rec_detail.value = [];
  };

  async function saveReceiptDetail() {
    try {
      // Get the current list of receipt details
      const res = await ReceiptDetailService.getReceiptDetails();
      const currentReceiptDetails = res.data;
      // Save or update the receipt detail depending on whether it has an id or not
      if (editReceiptDetail.value.id) {
        const res = await ReceiptDetailService.updateReceiptDetail(
          editReceiptDetail.value.id,
          editReceiptDetail.value
        );
      } else {
        const res = await ReceiptDetailService.saveReceiptDetail(
          editReceiptDetail.value
        );
      }

      dialog.value = false;
      await getReceiptDetails();
    } catch (e) {
      messageStore.showMessage("ไม่สามารถบันทึก Receipt Detail ได้");
    }
  }

  async function getReceiptDetails() {
    try {
      const res = await ReceiptDetailService.getReceiptDetails();
      editReceiptDetail.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Receipt Details ได้");
    }
  }
  return {
    rec_detail,
    editReceiptDetail,
    addReceiptDetail,
    AddMenu,
    DelMenu,
    deleteOrder,
    increment,
    decrement,
    allPrice,
    clearReceipt,
    saveReceiptDetail,
    getReceiptDetails,
  };
});
