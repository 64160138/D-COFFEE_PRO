import { ref } from "vue";
import { defineStore } from "pinia";
import type User from "@/Types/User";
import UserService from "@/services/user";
import { useMessageStore } from "@/stores/message";

export const useUserStore = defineStore("user", () => {
  const messageStore = useMessageStore();
  const isTable = ref(true);
  const editedUser = ref<User>({
    id: undefined,
    name: "",
    password: "",
    email: "",
  });
  const dialog = ref(false);
  const users = ref<User[]>([]);

  const login = (loginName: string, password: string): boolean => {
    const index = users.value.findIndex((item) => item.login === loginName);
    if (index >= 0) {
      const user = users.value[index];
      if (user.password === password) {
        return true;
      }
      return false;
    }
    return false;
  };

  const deleteUser = async (id: number): Promise<void> => {
    try {
      await UserService.deleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถลบ User ได้");
    }
  };

  const saveUser = async () => {
    try {
      if (editedUser.value.id !== undefined) {
        console.log(editedUser.value);
        const res = await UserService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const newUser = await UserService.saveUser(editedUser.value);
        editedUser.value.id = newUser.id;
      }

      dialog.value = false;
      await getUsers();
    } catch (e) {
      messageStore.showMessage("ไม่สามารถบันทึก User ได้");
      console.log(e);
    }
  };

  const clear = () => {
    editedUser.value = { id: undefined, name: "", password: "", email: "" };
  };

  const getUsers = async () => {
    try {
      const res = await UserService.getUsers();
      users.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล User ได้");
    }
  };

  const editUser = (user: User) => {
    editedUser.value = { ...user };
    dialog.value = true;
  };

  return {
    users,
    deleteUser,
    dialog,
    editedUser,
    clear,
    saveUser,
    editUser,
    isTable,
    getUsers,
    login,
  };
});
