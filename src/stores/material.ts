import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Stock from "@/Types/stock";
import type Materials from "@/Types/material";
import MaterialService from "@/services/material";
import { watch } from "vue";
export const useMaterialStore = defineStore("material", () => {
  const dialog = ref(false);
  const dialogCheck = ref(false);
  const stockList = ref<Materials[]>([]);
  const temp = ref();
  const select = ref();
  const editedstock = ref<Materials>({
    id: -1,
    name: "",
    unit: "",
    quantity: 0,
  });

  const clear = () => {
    editedstock.value = {
      id: -1,
      name: "",
      unit: "",
      quantity: 0,
    };
  };

  async function saveMaterial() {
    try {
      // Get the current list of Material
      const res = await MaterialService.getMaterials();
      const currentMaterial = res.data;
      // Save the Material
      const saveRes = await MaterialService.saveMaterial(editedstock.value);
      dialog.value = false;
      await getMaterials();
      clear();
    } catch (e) {
      //
    }
  }
  async function getMaterials() {
    try {
      const res = await MaterialService.getMaterials();
      stockList.value = res.data;
    } catch (e) {
      //
    }
  }

  const deleteMaterial = async (id: number): Promise<void> => {
    try {
      const res = await MaterialService.deleteMaterial(id);
      await getMaterials();
    } catch (e) {
      console.log(e);
      //
    }
  };

  watch(temp, (newTemp) => {
    if (newTemp) {
      editedstock.value = {
        id: temp.value.id,
        name: temp.value.name,
        unit: temp.value.unit,
        quantity: temp.value.quantity,
      };
    }
  });
  async function editMat(material: Materials) {
    editedstock.value = JSON.parse(JSON.stringify(material));
    if (editedstock.value.id) {
      const res = await MaterialService.updateMaterial(
        editedstock.value.id,
        editedstock.value
      );
    }
    //dialogCheck.value = false;
  }
  // watch(select, (newSelect) => {
  //   if (newSelect) {
  //     editedstock.value = {
  //       id: 0,
  //       name: "",
  //       unit: "",
  //       quantity: 0,
  //     };
  //   }
  // });
  const addCheck = async () => {
    const index = stockList.value.findIndex(
      (item) => item.id == editedstock.value.id
    );
    if (index === -1) {
      stockList.value.push(editedstock.value);
    } else {
      stockList.value[index].name = editedstock.value.name;
      stockList.value[index].unit = editedstock.value.unit;
      stockList.value[index].quantity = editedstock.value.quantity;
      const res = await MaterialService.updateMaterial(
        editedstock.value.id!,
        editedstock.value
      );
    }

    dialogCheck.value = false;
  };
  return {
    dialog,
    dialogCheck,
    editedstock,
    stockList,
    clear,
    deleteMaterial,
    getMaterials,
    saveMaterial,
    temp,
    editMat,
    addCheck,
  };
});
