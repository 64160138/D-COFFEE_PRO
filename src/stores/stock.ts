import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Stock from "@/Types/stock";
import StockService from "@/services/stock";
import { useMessageStore } from "./message";
import stock from "@/services/stock";

export const useStockStore = defineStore("stock", () => {
  const dialog = ref(false);

  const messageStore = useMessageStore();
  const stock = ref<Stock[]>();
  const editedStock = ref<Stock>({
    codeSto: "",
    name: "",
    quantity: 0,
    unit: "",
    price: 0,
  });
  const clear = () => {
    editedStock.value = {
      codeSto: "",
      name: "",
      quantity: 0,
      unit: "",
      price: 0,
    };
  };

  async function saveStock() {
    console.log(editedStock);
    try {
      if (editedStock.value.id) {
        const res = await StockService.updateStock(
          editedStock.value.id,
          editedStock.value
        );
      } else {
        const res = await StockService.saveStock(editedStock.value);
      }
      dialog.value = false;
      await getStocks();
    } catch (e) {
      messageStore.showMessage("ไม่สามารถบันทึก Stocks ได้");
    }
  }

  const deleteStock = async (id: number): Promise<void> => {
    try {
      await getStocks();
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถลบ Stocks ได้");
    }
  };
  async function getStocks() {
    try {
      const res = await StockService.getStocks();
      stock.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Stocks ได้");
    }
  }
  function editSto(stock: Stock) {
    editedStock.value = JSON.parse(JSON.stringify(stock));
    dialog.value = true;
  }
  return {
    stock,
    editSto,
    dialog,
    editedStock,
    clear,
    saveStock,
    deleteStock,
    getStocks,
  };
});
function getStocks() {
  throw new Error("Function not implemented.");
}
