import { defineStore } from "pinia";
import { computed, ref } from "vue";
import { useCoffeeStore } from "./menu";
import OrderService from "@/services/order";
import { useMessageStore } from "./message";
import { useCustomerStore } from "./customer";
import type Product from "@/Types/Product";
import { useLoadingStore } from "./loading";
import { useAuthStore } from "./auth";
import type { Order } from "@/Types/Order";
import { mdiPhone } from "@mdi/js";

export const useOrderStore = defineStore("order", () => {
  const dialog = ref();
  const messageStore = useMessageStore();
  const useCustomer = useCustomerStore();
  const loadingStore = useLoadingStore();
  const authStore = useAuthStore();
  const orders = ref<Order[]>([]);
  const orderList = ref<{ product: Product; amount: number; total: number }[]>(
    []
  );
  function addProduct(item: Product) {
    for (let i = 0; i < orderList.value.length; i++) {
      if (orderList.value[i].product.id === item.id) {
        orderList.value[i].amount++;
        orderList.value[i].total = orderList.value[i].amount * item.price;
        return;
      }
    }
    orderList.value.push({ product: item, amount: 1, total: 1 * item.price });
  }
  function clearOrder() {
    orderList.value = [];
  }
  function deleteProduct(index: number) {
    orderList.value.splice(index, 1);
  }
  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].total;
    }
    return sum;
  });
  const sumAmount = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].amount;
    }
    return sum;
  });
  async function openOrder() {
    const user: { id: number } = authStore.getUser();
    const orderItems = orderList.value.map(
      (item) =>
        <
          {
            productId: number;
            amount: number;
          }
        >{
          productId: item.product.id,
          amount: item.amount,
        }
    );

    const order = {
      orderItems: orderItems,
      payment: "เงินสด",
      received: 100,
      tel: "0864945419",
      storeId: 1,
      employeeId: user.id,
      customerId: 1,
    };
    loadingStore.isLoading = true;
    try {
      const res = await OrderService.saveOrder(order);
      dialog.value = false;

      await getOrders();
    } catch (e) {
      messageStore.showMessage("ไม่สามารถบันทึก Order ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function getOrders() {
    try {
      const res = await OrderService.getOrders();
      orders.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Customers ได้");
    }
  }

  const discountPrice = computed((phone: string) => {
    const customerStore = useCustomerStore();
    const discount = customerStore.checkPhone(phone) ? 0.05 : 0;
    return sumPrice.value * (1 - discount);
  });

  return {
    orderList,
    addProduct,
    deleteProduct,
    sumPrice,
    sumAmount,
    openOrder,
    clearOrder,
    getOrders,
    orders,
    discountPrice,
  };
});
