import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import router from "@/router";

export const useAuthStore = defineStore("auth", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authName = ref("");
  const acc = ref("");
  const account = ref();
  const getUser = () => {
    const userString = localStorage.getItem("user");
    if (!userString) return null;
    const user = JSON.parse(userString ?? "");
    return user;
  };
  function load() {
    const tmp = localStorage.getItem("user");
    acc.value = tmp!;
    account.value = JSON.parse(acc.value);
    console.log(account);
    console.log(acc.value);
  }
  const isLogin = () => {
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  };
  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(username, password);
      console.log(res.data.user);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);

      router.push("/");
    } catch (e) {
      messageStore.showMessage("Username OR Password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;
  };
  const logout = () => {
    // authName.value = "";
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    router.replace("/login");
  };

  return { login, logout, isLogin, account, load, getUser };
});
