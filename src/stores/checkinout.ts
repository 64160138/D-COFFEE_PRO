import { ref } from "vue";
import auth from "@/services/auth";
import { defineStore } from "pinia";
import type { checkinout } from "@/Types/check-in-out";
import { useMessageStore } from "./message";
import CheckInOutService from "@/services/checkinout";
import { useEmployeeStore } from "./employee";
import { useUserStore } from "./user";
import employee from "@/services/employee";
import { useAuthStore } from "./auth";
export const useCheckinoutStore = defineStore("checkinout", () => {
  const dialogTime = ref(false);
  const acco = useAuthStore();
  const messageStore = useMessageStore();
  const useEmployee = useEmployeeStore();
  const useUser = useUserStore();
  const checkinout = ref<checkinout[]>([]);
  const Auth = useAuthStore();
  const editedCheckInOut = ref<checkinout>({
    email: "",
    status: "",
  });
  const clear = () => {
    editedCheckInOut.value = {
      email: "",
      status: "",
    };
  };

  const saveCheckInOut = async () => {
    console.log(editedCheckInOut.value);

    try {
      if (editedCheckInOut.value.id !== undefined) {
        console.log(editedCheckInOut.value);
        const res = await CheckInOutService.updateCheckinout(
          editedCheckInOut.value
        );
      } else {
        const newCheckInOut = await CheckInOutService.saveCheckinout(
          editedCheckInOut.value
        );
        // editedCheckInOut.value.id = newCheckInOut.id;
      }

      dialogTime.value = false;
      await getCheckInOuts();
    } catch (e) {
      messageStore.showMessage("ไม่สามารถบันทึก CheckInOut ได้");
      console.log(e);
    }
  };

  const updatecheckout = async () => {
    try {
      const res = await CheckInOutService.updateCheckinout(
        editedCheckInOut.value
      );
      dialogTime.value = false;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถบันทึก CheckInOut ได้");
      console.log(e);
    }
    await getCheckInOuts();
  };

  const getCheckInOuts = async () => {
    try {
      const res = await CheckInOutService.getCheckinoutByEmp(
        Auth.account.email
      );
      checkinout.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล CheckInOut ได้");
      console.log(e);
    }
  };

  return {
    dialogTime,
    checkinout,
    editedCheckInOut,
    clear,
    saveCheckInOut,
    updatecheckout,
    getCheckInOuts,
  };
});
