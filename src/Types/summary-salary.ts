export default interface SummarySalary {
  id: number;
  date: string;
  workhour: number;
  salary: number;
}
