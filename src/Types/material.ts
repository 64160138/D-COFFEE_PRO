export default interface Materials {
  id?: number;
  name: string;
  unit: string;
  quantity: number;
}
