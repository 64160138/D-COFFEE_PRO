export default interface Stock {
  id?: number;
  codeSto?: string;
  name: string;
  quantity: number;
  unit: string;
  price: number;
}
