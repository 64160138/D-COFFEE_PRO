export default interface Employee {
  id?: number;
  name: string;
  address: string;
  phone: string;
  email: string;
  position: string;
  hourly_wage: number;
}
