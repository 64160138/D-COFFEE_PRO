export interface ReceiptDetail {
  id?: number;
  name: string;
  amount: number;
  price: number;
  total: number;
  type: string;
  category: string;
}
