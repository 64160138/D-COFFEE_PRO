export interface checkinout {
  id?: number;
  email: string;
  status: string;
  date?: Date;
  time_in?: string;
  time_out?: string;
  total_hour?: string;
}
