import type Stock from "@/Types/stock";
import http from "./axios";
function getStocks() {
  return http.get("/products");
}
function saveStock(stock: Stock) {
  return http.post("/products", stock);
}
function updateStock(id: number, stock: Stock) {
  return http.patch(`/products/${id}`, stock);
}
function deleteStock(id: number) {
  return http.delete(`/products/${id}`);
}
export default { getStocks, saveStock, updateStock, deleteStock };
