import type { checkinout } from "@/Types/check-in-out";
import http from "./axios";
function getCheckinout() {
  return http.get("/check-in-out");
}

function saveCheckinout(checkinout: checkinout) {
  return http.post("/check-in-out", checkinout);
}

function updateCheckinout(checkinout: checkinout) {
  return http.patch(`/check-in-out/`, checkinout);
}

function deleteCheckinout(id: number) {
  return http.delete(`/check-in-out/${id}`);
}

function getCheckinoutByEmp(email: string) {
  return http.get(`/check-in-out/email/${email}`);
}

export default {
  getCheckinout,
  saveCheckinout,
  updateCheckinout,
  deleteCheckinout,
  getCheckinoutByEmp,
};
