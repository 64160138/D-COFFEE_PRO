import type Store from "@/Types/store";
import http from "./axios";
function getStores() {
  return http.get("/store");
}
function saveStore(store: Store) {
  return http.post("/store", store);
}
function updateStore(id: number, store: Store) {
  return http.patch("/store/${id}", store);
}
function deleteStore(id: number) {
  return http.delete("/store/${id}");
}
export default { getStores, saveStore, updateStore, deleteStore };
