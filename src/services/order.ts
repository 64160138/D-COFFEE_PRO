import type { Order } from "@/Types/Order";
import http from "./axios";

function getOrders() {
  return http.get("/orders");
}

function saveOrder(order: {
  orderItems: { productId: number; amount: number }[];
  payment: string;
  received: number;
  tel: string;
  storeId: number;
  employeeId: number;
  customerId: number;
}) {
  return http.post("/orders", order);
}

function updateOrder(id: number, order: Order) {
  return http.patch(`/orders/${id}`, order);
}

function deleteOrder(id: number) {
  return http.delete(`/orders/${id}`);
}

export default { getOrders, saveOrder, updateOrder, deleteOrder };
