import type Employee from "@/Types/Employee";
import http from "./axios";
function getEmployees() {
  return http.get("/employees");
}

function saveEmployee(employee: Employee) {
  return http.post("/employees", employee);
}

function updateEmployee(id: number, employee: Employee) {
  return http.patch(`/employees/${id}`, employee);
}

function deleteEmployee(id: number) {
  return http.delete(`/employees/${id}`);
}

function getEmployee_info() {
  return http.get("reports/employee_view");
}
export default {
  getEmployee_info,
  getEmployees,
  saveEmployee,
  updateEmployee,
  deleteEmployee,
};
