import type SummarySalary from "@/Types/summary-salary";
import http from "./axios";
function getSummarySalary() {
  return http.get("/summarysalarys");
}
function saveSummrySalary(Summarysalary: SummarySalary) {
  return http.post("/summarysalarys", Summarysalary);
}
function updateSummarySalary(id: number, Summarysalary: SummarySalary) {
  return http.patch(`/summarysalarys/${id}`, Summarysalary);
}
function deleteSummarySalary(id: number) {
  return http.delete(`/summarysalarys/${id}`);
}
export default {
  getSummarySalary,
  saveSummrySalary,
  updateSummarySalary,
  deleteSummarySalary,
};
