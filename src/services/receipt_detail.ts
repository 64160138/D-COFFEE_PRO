import type { ReceiptDetail } from "@/Types/receiptDetail";
import http from "./axios";

function getReceiptDetails() {
  return http.get("/receipt-details");
}

function saveReceiptDetail(receiptDetail: ReceiptDetail) {
  return http.post("/receipt-details", receiptDetail);
}

function updateReceiptDetail(id: number, receiptDetail: ReceiptDetail) {
  return http.patch(`/receipt-details/${id}`, receiptDetail);
}

function deleteReceiptDetail(id: number) {
  return http.delete(`/receipt-details/${id}`);
}

export default {
  getReceiptDetails,
  saveReceiptDetail,
  updateReceiptDetail,
  deleteReceiptDetail,
};
